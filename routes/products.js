const express = require('express')
const router = express.Router()
const Product = require('../models/Product')
const products = [
  { id: 1, name: 'Ipad gen1 64G Wifi', price: 11000.0 },
  { id: 2, name: 'Ipad gen2 64G Wifi', price: 12000.0 },
  { id: 3, name: 'Ipad gen3 64G Wifi', price: 13000.0 },
  { id: 4, name: 'Ipad gen4 64G Wifi', price: 14000.0 },
  { id: 5, name: 'Ipad gen5 64G Wifi', price: 15000.0 },
  { id: 6, name: 'Ipad gen6 64G Wifi', price: 16000.0 },
  { id: 7, name: 'Ipad gen7 64G Wifi', price: 17000.0 },
  { id: 8, name: 'Ipad gen8 64G Wifi', price: 18000.0 },
  { id: 9, name: 'Ipad gen9 64G Wifi', price: 19000.0 },
  { id: 10, name: 'Ipad gen10 64G Wifi', price: 20000.0 }
]
const lastId = 11
const getProducts = async function (req, res, next) {
  try {
    const product = await Product.find({}).exec()
    res.status(200).json(product)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getProduct = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found!!'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
const addProducts = async function (req, res, next) {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const updateProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
const deleteProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }

  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product ' + req.params.id
  //   })
  // }
}

router.get('/', getProducts)
router.get('/:id', getProduct)
router.post('/', addProducts)
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)
module.exports = router
